import gensim
import numpy as np
import pymorphy2


def preprocessing(mas):
    """Запускает предобработку данных, возвращает np.array."""
    morph = pymorphy2.MorphAnalyzer()
    ls_train = []
    for line in mas.astype(str).str.split():
        words = []
        for word in line:
            lem = morph.parse(word)[0]  # делаем разбор
            words.append(lem.normal_form)
        ls_train.append(words)

    for ind in range(len(ls_train)):
        ls_train[ind] = ls_train[ind][0:28]

    x_embedded = []
    model1 = gensim.models.KeyedVectors.load('../model/181/model.model')
    for word in ls_train:
        x_embedded.append(model1[word])

    x_1 = np.array(x_embedded)
    x_11 = []
    for i in range(len(x_1)):
        size = 28 - x_1[i].shape[0]
        x_11.append(np.pad(x_1[i], ((0, size), (0, 0)), 'constant', constant_values=(0)))

    x_11 = np.array(x_11)

    x_11 = np.expand_dims(x_11, axis=3)
    return x_11
