from typing import List

import vk_api


def vk_posts(group_link, post_count, phone_number, password) -> List:
    """Ищет vk посты."""
    vk_session = vk_api.VkApi(phone_number, password)
    vk_session.auth()
    vk = vk_session.get_api()
    texts = []
    post = vk.wall.get(domain=group_link, count=post_count)
    for text in post['items']:
        texts.append(text['text'])
    return texts
