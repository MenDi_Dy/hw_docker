from http import HTTPStatus

import pytest
from aiohttp import web

from app.api.routes import setup_routes
from app.setup_service.configurator import get_config


@pytest.fixture
def cli(loop, aiohttp_client):
    """Тестовый клиент aiohttp."""
    app = web.Application()
    setup_routes(app)
    app['config'] = get_config([])
    return loop.run_until_complete(aiohttp_client(app))

# Требуется подключение к БД на другом handler и вход в vk


async def test_handle_without_match_info(cli):
    """Функция для тестирования."""
    resp = await cli.get('/')
    assert resp.status == HTTPStatus.OK.values


async def test_version(cli):
    """Функция для тестирования."""
    resp = await cli.get('/version')
    assert resp.status == HTTPStatus.OK.value

    resp_data = await resp.json()
    assert cli.app['config']['service']['version'] == resp_data['version']
