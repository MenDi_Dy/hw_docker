import aiohttp_jinja2
import asyncpg
from aiohttp import web

from model.NLP_model import nlp_model
from vk.vk_pars import vk_posts


@aiohttp_jinja2.template('index.html')
async def handler(request):
    """Инициализирует html форму."""
    return {}


@aiohttp_jinja2.template('result.html')
async def predict(request) -> dict:
    """Возвращает html форму с сентиментами для поста."""
    data = await request.post()
    group_link = data['Group_link']
    count = int(data['Post_count'])
    phone_number = str(data['Phone_number'])
    password = str(data['Password'])
    post = vk_posts(group_link, count, phone_number, password)
    res = nlp_model(group_link, count, phone_number, password)
    str_sen = str(", ".join(map(str, res)))
    text = ({
        'Phone_number': phone_number,
        'password': password,
        'Group_link': group_link,
        'count': count,
        'post': post,
        'res': res,
    })
    return text


async def version(request: web.Request) -> web.Response:
    """Возвращает номер версии сервиса."""
    config = request.app['config']
    return web.Response(text=config['service']['version'])
