from src.app.api.handlers import handler, predict, version


def setup_routes(app):
    """Инициализирует routes для приложения."""
    app.router.add_route('GET', '/', handler)
    app.router.add_route('POST', '/predict', predict)
    app.router.add_route('GET', '/version', version)
